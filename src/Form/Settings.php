<?php

namespace Drupal\notify_me\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this site.
 */
class Settings extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
    public function getFormId()
    {
        return 'notify_me_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
      'notify_me.settings',
    ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('notify_me.settings');
        $form['new_user_message'] = [
      '#type' => 'fieldset',
      '#title' => t('New User Email Notification'),
      '#weight' => 0,
      '#collapsible' => true,
      '#collapsed' => true,
    ];
        $form['new_user_message']['send_notification_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send notification'),
      '#description' => $this->t('Enable/Disable notification on user registration'),
      '#default_value' => $config->get('send_notification_check'),
    ];

        $form['new_user_message']['register_email_list'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email list to send the notification to'),
      '#description' => $this->t('An email list to notify in case of new user been created'),
      '#default_value' => $config->get('register_email_list'),
    ];
        $form['new_user_message']['extra_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message Body'),
      '#description' => $this->t('Message Body overwrite message about the user details, if this is empty, the custom message would go out<br>
      <br>Tokens can be used:<br>userEmail, userName, userId, siteName,'),
      '#default_value' => $config->get('extra_message'),
    ];

        // New Contents
        $form['new_content_message'] = [
      '#type' => 'fieldset',
      '#title' => t('New Content/Article Email Notification'),
      '#weight' => 1,
      '#collapsible' => true,
      '#collapsed' => true,
    ];
        $form['new_content_message']['send_content_notification_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send notification'),
      '#description' => $this->t('Enable/Disable notification on new published contents.'),
      '#default_value' => $config->get('send_content_notification_check'),
    ];

        $form['new_content_message']['register_content_email_list'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email list to send the notification to'),
      '#description' => $this->t('An email list to notify in case of new user been created'),
      '#default_value' => $config->get('register_content_email_list'),
    ];
        $form['new_content_message']['extra_content_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message Body'),
      '#description' => $this->t('Message Body overwrite original message about the new Content, if this is empty, the custom message would go out<br>
      <br>Tokens can be used:<br>authorEmail, authorName, nodeId, siteName, nodeURL'),
      '#default_value' => $config->get('extra_content_message'),
    ];
    
    

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Retrieve the configuration.
        $this->config('notify_me.settings')
        // Set the submitted configuration setting.
        ->set('send_notification_check', $form_state->getValue('send_notification_check'))
        ->set('register_email_list', $form_state->getValue('register_email_list'))
        ->set('extra_message', $form_state->getValue('extra_message'))
        ->set('send_content_notification_check', $form_state->getValue('send_content_notification_check'))
        ->set('register_content_email_list', $form_state->getValue('register_content_email_list'))
        ->set('extra_content_message', $form_state->getValue('extra_content_message'))
        ->save();

        parent::submitForm($form, $form_state);
    }
}
