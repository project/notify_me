<?php

namespace Drupal\notify_me\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines HelloController class.
 */
class Notify_Me_TestPage extends ControllerBase
{

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
    public function content()
    {
        $extra_content_message = \Drupal::config('notify_me.settings')->get('extra_content_message');
        $extra_message = \Drupal::config('notify_me.settings')->get('extra_message');

        return [
      '#type' => 'markup',
      '#markup' => 'User Notification email looks like below <br>'. $extra_message . ' Nodes email as below <br>' . $extra_content_message,
       ];
    }
}
