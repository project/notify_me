<?php
/**
 * NotifyMe module.
 */

use Drupal\user\UserInterface;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_page_attachments_alter().
 * @param array $attachments
 */
function notify_me_page_attachments_alter(array &$attachments)
{
    foreach ($attachments['#attached'] as $keyName  => $KeyValue) {
        // Replace http with https
        foreach ($attachments['#attached'][$keyName] as $key  => $value) {
            if (isset($value[0]['#attributes']['href'])) {
                $attachments['#attached'][$keyName][$key][0]['#attributes']['href'] = str_replace('http:', 'https:', $value[0]['#attributes']['href']);
            }
            if (isset($value[0]['#attributes']['content'])) {
                $attachments['#attached'][$keyName][$key][0]['#attributes']['content'] = str_replace('http:', 'https:', $value[0]['#attributes']['content']);
            }
        }
        if (!isset($attachments['#attached'][$keyName])) {
            return;
        }
        $remove_html_head_link = [
            'shortcut icon',
            'canonical'
        ];
        foreach ($attachments['#attached'][$keyName] as $key  => $value) {
            if (isset($value[0]['rel']) && in_array($value[0]['rel'], $remove_html_head_link)) {
                unset($attachments['#attached'][$keyName][$key]);
            }
            if (isset($value[0]['name']) && in_array($value[0]['name'], $remove_html_head_link)) {
                unset($attachments['#attached'][$keyName][$key]);
            }
        }
    }
}

/**
 * Implements hook_module_implements_alter().
 */
function notify_me_module_implements_alter(&$implementations, $hook)
{
    if ($hook  ===  'page_attachments_alter') {
        $group = $implementations['notify_me'];
        unset($implementations['notify_me']);
        $implementations['notify_me'] = $group;
    }
}


/**
 * Implements hook_ENTITY_TYPE_insert() for user entities.
 *
 * Notification after New user joined.
 */
function notify_me_user_insert(UserInterface $user)
{
    //check if the Notification module for new users is on
    $check = \Drupal::config('notify_me.settings')->get('send_notification_check');
    if ($check == true) {
        $email_list = \Drupal::config('notify_me.settings')->get('register_email_list');
        $extra_message = \Drupal::config('notify_me.settings')->get('extra_message');
        $site_name = \Drupal::config('system.site')->get('name');
        $params = [];
        $params['langcode'] = 'en';
        $params['to'] = $email_list;
        $params['from'] = \Drupal::config('system.site')->get('mail');
        $params['message'] = 'New user just been created with email '. $user->mail->value . ', username '. $user->name->value .' on ' . $site_name . '<br>';
        // Check if there is a custom text instead
        if ($extra_message !== '') {
            $token = ["userEmail", "userName", "userId", "siteName"];
            $result_token = [$user->mail->value, $user->name->value, $user->uid->value, $site_name];
            $params['message'] = str_replace($token, $result_token, $extra_message);
        }
    
        $params['subject'] = 'New User just been created on ' . $site_name;
        $params['title'] = 'New User just been created on ' . $site_name;
        $params['key'] = 'new_user';
        $params['send'] = true;
        $params['userId'] = $user->uid->value;
        // Now submit the mail to go out
        $mailManager = \Drupal::service('plugin.manager.mail');
        $module = 'notify_me';
        $key = 'create_user';
        $params['node_title'] = '';
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $send = true;
        \Drupal::logger('notify_me')->warning(print_r($params, true));
        $result = $mailManager->mail($module, $key, $email_list, $langcode, $params, null, $send);
        if ($result['result'] !==  true) {
            \Drupal::logger('notify_me')->warning(print_r($params, true));
            \Drupal::logger('notify_me')->warning(print_r($result, true));
        } else {
            drupal_set_message(t('Your message has been sent.'));
        }
    }
}

/**
* Implements hook_entity_insert().
*/
function notify_me_entity_insert(Drupal\Core\Entity\EntityInterface $entity)
{
    //check if the Notification module for new users is on
    $check = \Drupal::config('notify_me.settings')->get('send_content_notification_check');
    if ($check !== true || $entity->getEntityTypeId() !== 'node' || ($entity->getEntityTypeId() === 'node' && $entity->bundle() !== 'article')) {
        return;
    }
    $email_list = \Drupal::config('notify_me.settings')->get('register_content_email_list');
    $extra_message = \Drupal::config('notify_me.settings')->get('extra_content_message');
    $site_name = \Drupal::config('system.site')->get('name');
    $params = [];
    $params['langcode'] = 'en';
    $params['to'] = $email_list;
    $params['from'] = \Drupal::config('system.site')->get('mail');
    $params['message'] = $entity->get('body')->value;
    // Check if there is a custom text instead
    if ($extra_message !== '') {
        $token = ["nodeTitle", "nodeBody", "nodeAuthor", "siteName"];
        $result_token = [$entity->label(), $entity->get('body')->value, $entity->get('author')->value, $site_name];
        $params['message'] = str_replace($token, $result_token, $extra_message);
    }
    $params['subject'] = $site_name . ' | ' . $entity->label();
    $params['message'] = $entity->get('body')->value;
    $params['node_title'] = $site_name . ' | ' . $entity->label();
    $params['key'] = 'new_article';
    $params['send'] = true;
    // Now submit the mail to go out
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'notify_me';
    $key = 'create_user';
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = $params['send'];
    try {
        $result = $mailManager->mail($module, $key, $email_list, $langcode, $params, null, $send);
        \Drupal::logger('notify_me')->notice(print_r($result, true));
    } catch (\Throwable $th) {
        \Drupal::logger('notify_me')->error(print_r($result, true));
    }
}

/**
* Implements hook_mail().
*/
function notify_me_mail($key, &$message, $params)
{
    $options['langcode'] = $params['langcode'];
    switch ($params['key']) {
    case 'new_user':
        try {
            // Set the the format to HTML.
            $message['format'] = 'text/html';
            // Allow the body to rendered.
            $message['from'] = $params['from'];
            $message['subject'] = t($params['subject'], $options);
            $message['body'] = Markup::create($params['message'], $options);
            break;
        } catch (\Throwable $th) {
            \Drupal::logger('notify_me')->error(print_r($params, true));
        }
      
  }
}
